<section class="portfolio">
    <div class="content col-md-10">
        <header>Portfolio</header>
        <div class="port-content">
        @if (!empty($portfolio))
            @foreach($portfolio AS $port)
                @if (!empty($port['images']))
                    <div class="portfolio-container">
                        <?php $i = 0; 
                        $last = count($port['images']);
                        ?>
                        @foreach($port['images'] AS $image)
                            @if ($i == 0) 
                                <a @if (!empty($image)) href="/media/portfolio/{{ $image }}" 
                                    data-lightbox="image-{{ $port['id'] }}" @endif>
                                        {{ $port['title'] }}
                                </a>
                                <!-- <div class="images-container"> -->
                            @else
                                @if (!empty($image))
                                    <!--- {{ $image }} --->
                                    <a href="/media/portfolio/{{ $image }}" class="hidden"
                                        data-lightbox="image-{{ $port['id'] }}"></a>
                                @endif
                            @endif
                            <?php $i++; ?>
                            @if ($i == $last) 
                            <!-- </div> -->
                            @endif
                        @endforeach
                    </div>
                @endif
            @endforeach
        @endif 
        </div>
    </div>
</section>

