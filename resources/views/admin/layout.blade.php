<!Doctype html>
<html lang="en">
<head>
    <title>{{ $pagetitle or '' }}</title>
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="/css/jquery-ui-1.10.0.custom.min.css" />
    <link rel="stylesheet" href="/css/admin.css" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.10.0.custom.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>

</head>

<body>

<div class="main-container" @if (isset($ng_app)) ng-app="{{ $ng_app }}" @endif>
    <div class="main-wrapper clearfix">
        @include('admin.leftnav') 
        <div class="content-container">
        @yield("content")

        </div>

    </div>
</div>

<script>

$(function() {
    $( ".accordion" ).accordion({ active:false });
});
</script>

@yield("resources")
</body>
</html>
