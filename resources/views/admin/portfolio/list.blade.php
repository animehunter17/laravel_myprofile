@extends("admin.layout")

@section("pagetitle", $pagetitle )

@section("content")
<div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
@if (session("message")) {!! session("message") !!} @endif

@if (isset($list))
    @foreach($list AS $item)
        <div class="list">
            <!-- <div class="img-thumb"><img src="/public/media/{{ $item->port_img }}" style="max-width:200px;" /></div> -->
            <div><strong>{{ $item->port_title }}</strong></div>
            <div><a href="{{ $item->port_link }}" target="_blank">{{ $item->port_link }}</a></div>
            <div>{{ $item->position }}</div>
            <a href="/admin/portfolio/edit/{{ $item->port_id }}" class="btn btn-primary edit">Edit</a>
            <a href="/admin/portfolio/images/add-edit/{{ $item->port_id }}" class="edit">Add/Edit Images</a>
            <a href="#" class="delete">Delete</a>
        </div>
        

    @endforeach

@endif

@endsection
