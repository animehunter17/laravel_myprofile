@extends("admin.layout")

@section("pagetitle", $pagetitle )

@section("content")
<div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
@if (session("message")) <div class="alert alert-danger">{{ session("message") }}</div> @endif
<form method="POST" action="{{ $formaction }}">
{!! csrf_field() !!}
<!-- <div> -->
<!--     <div class="block150">Image <br/> -->
<!--         <span class="note">(480x260 or 960x520)</span> -->
<!--     </div> -->
<!--     <input type="file" name="image" class="block200" /> -->
<!-- </div> -->
<div>
    <div class="block150">Title</div>
    <input type="text" name="title" class="block200" @if (isset($item->port_title)) value="{{ $item->port_title }}" @endif />
</div>
<div>
    <div class="block150">Link</div>
    <input type="text" name="link" class="block200" @if (isset($item->port_link)) value="{{ $item->port_link}}" @endif />
</div>
<div>
    <div class="block150">Position</div>
    <input type="text" name="position" class="block200" @if (isset($item->position)) value="{{ $item->position }}" @endif />
</div>
@if (isset($item))
<input type="hidden" name="id" value="{{ $item->port_id }}" />
<button type="submit" class="btn btn-primary"> Edit </button> 
@else
<button type="submit" class="btn btn-primary"> Add </button> 
    
@endif
</form>
@endsection
