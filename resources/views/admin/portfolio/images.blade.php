@extends("admin.layout")

@section("pagetitle", $pagetitle )

@section("resources")
@if (isset($js))
    @foreach($js AS $jscript)
        <script src="{{ $jscript }}"></script>
    @endforeach
@endif

@endsection

@section("content")

<div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
@if (session('message')) {!! session('message') !!} @endif

@if (isset($item))
    <div>{{ $item->title}}</div>
@endif


<form method="POST" action="{{ $formaction }}" enctype="multipart/form-data" class="image-container">
{!! csrf_field() !!}
<div>
    <label>Add image: </label><input type="file" name="port_image" />
</div>
<input type="hidden" name="portfolioId" value="{{ $id }}" />
<button type="submit" class="btn btn-primary">Add</button>
</form>

Images
<div class="images-container">
@if (isset($images))
    @forelse($images AS $image)
        <div class="fp-image-container" data-photo-id="{{ $image->id }}">
            <img src="/media/portfolio/{{ $image->encrypt_name }}" />
                <!-- <button class="btn btn-primary make-primary">Make Primary</button><br /> -->
                <button class="btn btn-danger delete-img">Delete</button>
        </div>
    @empty
        <p>No images</p>
    @endforelse

@endif
</div>
<a href="/admin/portfolio/listing" class="btn btn-primary">Back</a>

@endsection
