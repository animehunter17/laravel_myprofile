@extends("admin.layout")

@section("pagetitle", $pagetitle )

@section("resources")

@if (isset($js))
    @foreach($js AS $jscript)
        <script src="{{ $jscript }}"></script>
    @endforeach
@endif

@endsection

@section("content")
<div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
<form method="POST" action="{{ $formaction }}" enctype="multipart/form-data">
{!! csrf_field() !!}
@if (session('message')) {!! session('message') !!} @endif
@if (isset($item->image)) 
    <img src="/media/{{ $item->image }}" style="max-height:150px;" />
    <input type="hidden" name="oldimage" value="{{ $item->image }}" />
@endif
<div>
    <div class="block150">upload new image:</div>
    <input type="file" name="newimage" />
</div>
<div>
    
    <input type="hidden" name="id" value="@if (isset($item->id)) {{ $item->id }} @endif" />
    <div class="block150">Title</div>
    <input type="text" class="block200" name="title" @if(isset($item->title)) value="{{ $item->title }}" @endif />
</div>
<div>
    <div class="block150">Sub title</div>
    <input type="text" class="block200" name="subtitle" @if(isset($item->sub_title)) value="{{ $item->sub_title }}" @endif />
</div>
<div>
    <div class="block150">Front page description</div>        
    <textarea rows="5" name="desc1">@if (isset($item->desc1)){{ $item->desc1 }}@endif</textarea>
</div>
<div>
    <div class="block150">Second page description</div>        
    <textarea rows="5" name="desc2">@if (isset($item->desc2)){{ $item->desc2 }}@endif</textarea>
</div>

<button type="submit" class="btn btn-small btn-primary">@if (isset($item)) Edit @else Add @endif</button>
</form>
<a href="/admin/frontpage/list" class="btn btn-small btn-primary">Go Back</a>

<script>
$(function(){
CKEDITOR.replace("desc2");
});
</script>

@endsection
