@extends("admin.layout")

@section("pagetitle", $pagetitle )

@section("resources")
@if (isset($js))
    @foreach($js AS $jscript)
        <script src="{{ $jscript }}"></script>
    @endforeach
@endif

@endsection

@section("content")

<div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
@if (session('message')) {!! session('message') !!} @endif

@if (isset($item))
    <div>{{ $item->title}}</div>
@endif


<form method="POST" action="{{ $formaction }}" enctype="multipart/form-data">
{!! csrf_field() !!}
<div>
    <label>Add image: </label><input type="file" name="fpimage" />
</div>
<input type="hidden" name="fpId" value="{{ $id }}" />
<button type="submit" class="btn btn-primary">Add</button>
</form>

Images
<div class="images-container">
@if (isset($images))
    @forelse($images AS $image)
        <div class="fp-image-container">
            <a href="/media/fpimages/{{ $image->encrypt_name }}" target="_blank">
                <img src="/media/fpimages/{{ $image->encrypt_name }}" />
            </a>
            @if (!$image->primary)
                <button class="btn btn-primary make-primary" data-photo-id="{{ $image->photo_id }}">Make Primary</button>
            @endif
        </div>
    @empty
        <p>No images</p>
    @endforelse

@endif
</div>

@endsection
