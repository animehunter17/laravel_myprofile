@extends("admin.layout")

@section("pagetitle", $pagetitle )
@section("resources")

@if (isset($js))
    @foreach($js AS $jscript)
        <script src="{{ $jscript }}"></script>
    @endforeach
@endif

@endsection

@section("content")
<div ng-app="fpApp">
    <div ng-controller="fpController" class="fp-list">
        <meta name="csrf_token" content="{{ csrf_token() }}" />
        <div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
        @if (session('message')) {!! session('message') !!} @endif
        @if (isset($list))

            @foreach($list AS $listing)
                <div class="list_div" data-id="{{ $listing->id }}">
                    <div class="block200">{{ $listing->title }}</div>
                    <a href="/admin/frontpage/edit/{{ $listing->id }}" class="block150">Edit</a> 
                    <a href="/admin/frontpage/images/add-edit/{{ $listing->id }}" class="block150">Images</a> 
                    <a href="#" class="delete">Delete</a>
                </div>
            @endforeach

        @endif

        <input ng-model="testtext" value="<% txt %>" />


    </div>
</div>
@endsection
