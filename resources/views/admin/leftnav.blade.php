 <div class="left-nav accordion">
        @if ( isset($menus) )
            @foreach($menus AS $list)
                <h4><a href="#">{{ $list['link_name'] }}</a></h4>
                <div>
                    @if (isset($list['submenus']))
                        @foreach ($list['submenus'] AS $submenu)
                            <a href="{{ $submenu['url'] }}">{{ $submenu['link_name'] }}</a><br />
                        @endforeach
                    @endif
                </div>
            @endforeach
            <h4><a href="#">Menu</a></h4>
            <div>
                <a href="/admin/menu-setup">Menu Setup</a><br />
                <a href="/admin/menu-list">Menu List</a>
            </div>
        @endif
</div>
<a class="btn btn-small btn-primary" href="/admin/logout">Logout</a>
    
