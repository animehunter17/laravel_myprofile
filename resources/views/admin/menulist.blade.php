@extends("admin.layout")

@section("resources")
    @if (isset($js))
        @foreach($js AS $jscript)
            <script src="{{ $jscript }}"></script>
        @endforeach
    @endif
@endsection

@section("content")

<div id="parentlist" ng-controller="menuController">
@if (session('error'))
    <div>{{ session('error') }}</div>

@endif
@if (isset($menus))
    @foreach ($menus AS $menu)
            
    @endforeach

@endif
     <div class="menu_wrapper" ng-repeat="menu in menulist">
        <div class="clearfix title_parent">
            <div class="menu-title"><% menu.link_name %></div>
            <div class="go_left"><a class="show_parent" ng-click="showHideMenu(menu)">Show/Hide</a></div>
            <div class="go_left"><a class="edit_parent" ng-click="showMenuDialog(menu)">Edit</a></div>
            <div class="go_left"><a class="delete_parent">Delete</a></div>
            
        </div> <!--  End Title Parent -->
        
        <div class="sub-menu" ng-if="menu.show">
            <div class="clearfix list" ng-repeat="submenu in menu.submenus"> <!-- SUB MENU ROW -->
                <div class="sub-menu-title"><% submenu.link_name %></div>
                <a href="#" class="edit-submenu" ng-click="showSubDialog( submenu )">Edit</a>
                <a href="#" ng-click="deleteSubMenu( submenu.id )">Delete</a>
            </div>

        </div> <!-- End Sub Menu wrapper -->
    </div>

    <div id="parent_dialog" title="Edit Parent Menu"><!-- PARENT MENU DIALOG -->
        <!-- <form method="POST" action="/admin/update-menu"> -->
        {!! csrf_field() !!}
        <div class="clearfix">
            <div class="block150 go_left">Link Name: </div>
            <input type="text" id="parentname" name="link_name" ng-model="linkName" class="block200 go_left" />		
        </div>	
        <input type="hidden" name="menuid" />
        <!-- <button type="submit" class="btn btn-primary btn-small">Update</button> -->
        <button class="btn btn-primary" ng-click="updateMenu()">Update</button>
        <!-- </form> -->
    </div>


    <div id="sub_dialog" title="Edit Sub Menu"><!-- SUB MENU DIALOG -->
        <!-- <form method="POST" action="/admin/update-menu"> -->
        {!! csrf_field() !!}
        <div class="clearfix">
            <div class="block150 go_left">Link Name: </div>
            <input type="text" id="subname" name="link_name" ng-model="linkName" class="block200 go_left" />
        </div>
        <div class="clearfix">
            <div class="block150 go_left">Url: <br /><span class="note"><?php //echo base_url(); ?></span></div>
            <input type="text" id="suburl" name="suburl" ng-model="link_url" class="go_left block200" />
        </div>
        <input type="hidden" id="menuid" name="menuid" />
        <button class="btn btn-primary btn-small" ng-click="updateMenu()">Update</button>
        <!-- </form> -->
    </div>

</div>

@endsection

