@extends("admin.layout")

@section("pagetitle", $pagetitle)

@section("resources")
@if (isset($js))
    @foreach($js AS $jscript)
        <script src="{{ $jscript }}"></script>
    @endforeach
@endif
@endsection


@section("content")
@if (session('error'))
    <div>{{ session('error') }}</div>
@endif
<div class="page_title">@if (isset($pagetitle)) {{ $pagetitle }} @else Admin @endif</div>
<form method="POST" action="/admin/addMenu">
{!! csrf_field() !!}
    <div>
        <label class="block150">Menu Name: </label>
        <input type="text" name="menu_name" class="block200" />
    </div>
    <div>
        <label class="block150">Parent Menu? </label>
        <select id="parentmenu" name="parentmenu" onchange="submenu();" class="block150">
            <option>Yes</option>
            <option>No</option>		
        </select>
    </div>

    <div style="display:none;" id="submenu_div"><!-- Show Hide Sub Menu fields -->
        <div>
            <label class="block150">Which Parent Menu is this part of?</label>
            <select name="parent_id" class="block150">
                    <option value=""></option>
                    @if ( isset($menus) )
                        @foreach ($menus AS $list)
                            <option value="{{ $list['parent_id'] }}">{{ $list['link_name'] }}</option>
                        @endforeach
                    @endif
            </select>
        </div>
        <div>
            <label class="block150">Url: <br /><span class="note"><?php //echo base_url(); ?></span></label>
            <input type="text" name="linkurl" class="block200" />
        </div>
    </div>
	
    <button type="submit" class="btn btn-small btn-primary">Add</button>
</form>
<script>
//show/hide the dropdown option of all parent menus
function submenu(){
    var parent =$('#parentmenu').val();
    if(parent == "No")
		$('#submenu_div').slideDown();
    else 
		$('#submenu_div').slideUp();
    
}

</script>
@endsection


