<!doctype html>
<html lang="en">
<head>
<title>@if (isset($title)) {{ $title }} @endif</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1"  />
<!-- <link rel="stylesheet" href="//sinluong.com/css/style.css" /> -->
<link rel="stylesheet" href="//sinluong.com/css/twitter.css" />
<link rel="stylesheet" href="/css/lightbox.css" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="/css/home.css" media="screen" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="//store.sinluong.com/js/jquery.validate.min.js"></script>
</head>
<body>
<!-- <div class="wrapper"> -->
<ul class="nav nav-pills nav-stacked">
    @if (isset($fp))
        <?php $i = 1 ?>
        @foreach($fp AS $link) 
            <li role="presentation">
                <a class="link-{{ str_replace(" ", "-", $link->title) }} @if ($i==1) active @endif" data-id="{{ $i == 0 ? 1: $i }}" href="#">
                    {{ $link->title }}
                </a>
                <!-- <button class="link-{{ str_replace(" ", "-", $link->title) }}" data-id="{{ $i == 0 ? 1: $i }}">{{ $link->title }}</button> -->
            </li>
            <?php $i++ ?>
        @endforeach
    @endif
    <li role="presentation">
        <a href="#" class="link-portfolio" data-id="{{ $i }}">Portfolio</a>
    </li>
    <li role="presentation">
        <?php $i++; ?>
        <a href="#" class="link-twitter" data-id="{{ $i }}">Twitter</a>
    </li>
</ul>

@if (isset($fp))
    @foreach($fp AS $section)
        <section class="{{ str_replace(" ", "-", $section->title) }} clearfix">
            <div class="content col-md-10">
                <header>{{ $section->title }}</header>
                <!-- <div class="desc">{!! nl2br($section->desc2) !!}</div> -->
                <div class="desc">{!! $section->desc2 !!}</div>
                <div class="form-group"></div>
                @if (strtolower($section->title) == "contact")
                    @include("contact")
                @endif
            </div>
        </section>
        
    @endforeach
@endif

@include("portfolio")
@include("twitter")

<script src="/js/home.js"></script>
<script src="/js/lightbox.js"></script>
</body>
</html>

