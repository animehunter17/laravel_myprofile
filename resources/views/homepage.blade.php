<!doctype html>
<html lang="en">
<head>
<title>@if (isset($title)) {{ $title }} @endif</title>
<link rel="stylesheet" href="//sinluong.com/css/style.css" />
<link rel="stylesheet" href="//sinluong.com/css/twitter.css" />
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Ovo' rel='stylesheet' type='text/css' />                
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
</head>
<body>


<div class="container" id="container">

@if (isset($fp))

    @foreach ($fp as $list)
        <!-- <div class="item block" data-bgimage="media/images/frontpage/<?php //echo $list['image']; ?>"> -->
        <div class="item block" @if ($list->image) data-bgimage="/media/{{ $list->image }}" @endif >
            <div class="thumbs-wrapper">
                <div class="thumbs">                                            
                      <?php                                          
                        // foreach($list_photos AS $photos):
                        //     if($photos['windowID'] == $list['id']){ 
                        //         echo "<img src='media/images/frontpage/thumbs/".$photos['imgname']."' />"; 
                        //     }
                        // endforeach; ?>
                </div>
            
            </div>
            <h2 class="title">{{ $list->title }}</h2>
            <p class="subline">{{ $list->sub_title }}</p>
                <div class="intro">
                    <p>{{ $list->desc1 }}&nbsp;
                        @if ($list->title =="Photos")
                            <a href="photos">
                        @elseif ($list->title =="Portfolio")
                            <a href="portfolio">
                        @else
                            <a href="#" class="more_link">
                        @endif View More</a></p>
                </div>
                <div class="project-descr">
                    <div style="margin:10px 20px;">
                        <p><?php// echo nl2br($list['desc2']); ?></p>
                        <?php // if($list['title'] =="Contact"){ echo $contact_page; } ?>
                </div>
            </div>
        </div>

    @endforeach


@endif
     <div class="item tweetblock twitter_block">		
            <div id="twitter-ticker">
            <!-- Twitter container, hidden by CSS and shown if JS is present -->
                <div id="top-bar">
                <!-- This contains the title and icon -->
                    <div id="twitIcon"><img src="//sinluong.com/media/images/twitter/twitter_64.png" width="64" height="64" alt="Twitter icon" /></div>
                    <!-- The twitter icon -->
                        <h2 class="tut">My tweets</h2>
                        <!-- Title -->
                </div> 
                <div id="tweet-container">
					<?php
					if(is_array($twitter) && !empty($twitter)):
						foreach($twitter AS $tweets):
							// $tweets->text = str_replace("http".$link, "<a href='http".$link."'>".$link."</a>", $tweets->tweet);
							$tweets->text = preg_replace('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', "<a target='_blank' href=\"\\0\">\\0</a>",$tweets->text);
							echo "<li class='tweet-text'>";
							if($tweets->in_reply_to_screen_name){
								$reply_name = $tweets->in_reply_to_screen_name;
								echo str_replace("@$reply_name", "<a href='http://twitter.com/".$reply_name."' target='_blank'>@".$reply_name."</a>", $tweets->text);
							} elseif(isset($tweets->retweeted_status)) { //RETWEETS. make links clickable
								$retweet_text = $tweets->retweeted_status->text;
								$retweet_text = preg_replace('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', "<a target='_blank' href=\"\\0\">\\0</a>",$retweet_text);
								$reply_name = $tweets->retweeted_status->user->screen_name;
								$tweettext = "RT @$reply_name: ".$retweet_text;
								echo str_replace("@$reply_name", "<a href='http://twitter.com/".$reply_name."' target='_blank'>@".$reply_name."</a>", $tweettext);

							} else {
								echo $tweets->text;
							}
							echo "</li>";	
						endforeach;
					endif;
					?>
				</div>
                <!-- The loading gif animation - hidden once the tweets are loaded -->
                <div id="scroll"></div>
                <!-- Container for the tweets -->
            </div>
		</div>    



</div><!-- container -->
</body>
</html>
