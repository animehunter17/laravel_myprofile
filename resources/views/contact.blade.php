<form id="myform">
<meta name="csrf_token" content="{{ csrf_token() }}" />
<div class="form-group">
    <label>Email</label>
    <input type="email" name="email" class="form-control" placeholder="Email address" required />
</div>
<div class="form-group">
    <label>Message</label>
    <div class="contact-msg">
        <textarea class="form-control required" placeholder="Enter message here..." name="message" rows="6" required></textarea>
    </div>
</div>
<div class="checkbox">
    <label>
        <input type="checkbox" value="1" name="copy" /> Send me a copy
    </label>
</div>
<button class="btn btn-site-red sendmsg">Send</button>
</form>
