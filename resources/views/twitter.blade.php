<section class="twitter">
    <div class="content col-md-10">
        <header>Twitter</header>
        @if (!empty($twitter))
            <div class="carousel-inner" role="listbox">
                <?php $i = 0; $y = 0; ?>
                @foreach($twitter AS $feed)
                    @if ($y == 0)
                        <div class="twitter-container item @if ($i==0) active @endif">
                    @endif
                    <!-- tweets -->
                    <?php
                    $feed->text = preg_replace('!(http)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', "<a target='_blank' href=\"\\0\">\\0</a>",$feed->text);
                    ?>
                    <li class="tweet-text">

                        @if ($feed->in_reply_to_screen_name)
                            <?php
                            $reply_name = $feed->in_reply_to_screen_name;
                            ?>
                            {!! str_replace("@$reply_name", 
                                            "<a href='http://twitter.com/".$reply_name."' target='_blank'>@".$reply_name."</a>", 
                                            $feed->text) !!}
                        @elseif (isset($feed->retweeted_status)) {{-- //RETWEETS. make links clickable --}}
                            <?php
                            $retweet_text = $feed->retweeted_status->text;
                            $retweet_text = preg_replace('!(http)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', "<a target='_blank' href=\"\\0\">\\0</a>",$retweet_text);
                            $reply_name = $feed->retweeted_status->user->screen_name;
                            $tweettext = "RT @$reply_name: ".$retweet_text;
                            ?>
                            {!! str_replace("@$reply_name", 
                                            "<a href='http://twitter.com/".$reply_name."' target='_blank'>@".$reply_name."</a>", 
                                            $tweettext) !!}
                        @else
                            {!! $feed->text !!}
                        @endif
                    </li>

                    @if ($y == 3)
                        </div>
                    @endif
                    <?php 
                    $i++; 
                    $y++; 
                    if ($y == 4) $y=0;
                    ?>
                @endforeach
            </div>
        @endif

    </div>
</section>
