<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewAdminProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer("admin.leftnav", function($view){
            $menus = \App\Admin::all();
            $data = \App\Http\Controllers\Admin\HomeController::RebuildMenuList($menus->toArray());

            $view->with("menus", $data);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
