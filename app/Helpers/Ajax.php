<?php

namespace App\Helpers;

class Ajax {

    public static function is(){

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest")
            return true;

        echo "This Request is not allowed";
        exit;

    }

    public static function sendResponse($data=array(), $success=true){

        if (empty($data))
            echo json_encode(array("success" => false, "message" => "Something went wrong"));

        if (is_array($data) && !empty($data)){
            $data["success"] = $success;
            echo json_encode($data);
        }

        exit;


    }



}
