<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Frontpage extends Model
{
    //
    //
    protected $table = "frontpage";
    protected static $fpphoto_tb = "FPphotos";


    public static function addSubPhoto($data){

        // DB::table("FPphotos");

    }


    public static function getFPImages($id){

        return DB::table(self::$fpphoto_tb)->where("window_id", $id)->get();

    }

    public static function InsertFPImage($original_name, $encrypt_name, $window_id){

        return DB::table(self::$fpphoto_tb)->insert(["original_name" => $original_name, "encrypt_name" => $encrypt_name, "window_id" => $window_id]);

    }

    public static function setPrimaryImg($id){

        DB::table(self::$fpphoto_tb)->where("primary", 1)->update(["primary" => 0]);
        DB::table(self::$fpphoto_tb)->where("photo_id", $id)->update(["primary" => 1]);

    }

}
