<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use illuminate\Http\Request;
use Auth;
use App\Admin as AdminAction;
use App\Frontpage as FPModel;
use App\Libraries\UploadLib;
use DB;

class FrontpageController extends Controller {
    const FP_IMAGE_LIMIT = 5000000;

    public function __construct(){
        $this->middleware("auth");
    }


    public function addnew(){

        $data['formaction'] = "/admin/frontpage/addnew";
        $data['pagetitle'] = "Add new window";

        return view("admin.frontpage.addnew", $data);

    }

    public function processAddNew(Request $request){

        $required = array("title", "subtitle", "desc1", "desc2");
        $error = false;
        foreach($required AS $require):
            if (!$request->input($require)){
                $error = true;
                break;
            }
        endforeach;
        
        if($error)
            return redirect('/admin/frontpage/addnew')->with('message', $this->displayError('All fields are required'));


        $title = $request->input("title");
        $subtitle = $request->input("subtitle");
        $desc1 = $request->input("desc1");
        $desc2 = $request->input("desc2");

        // print_r($_POST);

        $insertdata = ["title" => $title, "sub_title" => $subtitle, "desc1" => $desc1, "desc2" => $desc2 ];
        DB::table("frontpage")->insert($insertdata);
        return redirect("admin/frontpage/list")->with("message", $this->displaySuccess("Item Added"));

    }

    public function edit($id){
        $data['item'] = FPModel::findOrfail($id);
        $data['formaction'] = "/admin/frontpage/update/$id";
        $data['pagetitle'] = "Window Edit";
        $data['js'][] = "/js/ckeditor/ckeditor.js";
        return view("admin.frontpage.addnew", $data);

    }

    public function processUpdate(Request $request){

        $id = $request->input("id");
        $FPupdate = array("title" => $request->input("title"),
                        "sub_title"  => $request->input("subtitle"),
                        "desc1" => $request->input("desc1"),
                        "desc2" => $request->input("desc2")
                    );
        // $newimage = $request->file('newimage');
        // echo "<pre>";
        // print_r($newimage);
        // echo "</pre>";
        // print_r($newimage->getFileName());
        // print_r($newimage->getClientOriginalExtension());
        // exit;
        // $image_name = false;
        // $Path = $_SERVER['DOCUMENT_ROOT']."/media/";

        //if image is selected
        // if ($newimage) {
        //     $image['original_name'] = $newimage->getClientOriginalName();
        //     $image_name = $newimage->getFileName();
        //     $timenow = time();
        //     $image['encrypt_name'] = $image_name."_".$timenow.".".$newimage->getClientOriginalExtension();
        //     $newimage->move($Path, $update['image']);
        // } 


        //delete old image if exists
        // $oldimage = $request->input("oldimage");
        // if ($oldimage && $image_name){
        //     if (file_exists($Path.$oldimage))
        //         unlink($Path.$oldimage);
        // }

        $rows = FPModel::where("id", $id)->update($FPupdate);

        if ($rows)
            return redirect("admin/frontpage/list")->with("message", $this->displaySuccess("Item updated successfully"));
        else
            return redirect('/admin/frontpage/list')->with('message', $this->displayError('Item cannot be updated'));

    }

    public function listing(){

        $data['list'] = FPModel::all();
        $data['pagetitle'] = "Window Listing";
        $data['js'] = array("/js/angular/app.js", "/js/angular/controllers/frontpage.js", "/js/frontpage.js");
        // $data['js'] = array("/js/angular/controllers/frontpage.js");

        return view("admin.frontpage.list", $data);

    }

    public function processDelete(Request $request){

        $id = $request->input("id");
        $rows = FPModel::where("id", $id)->delete();

        $response = ($rows) 
                    ? array("success" => true, "message" => "Item deleted successfully")
                    : array("success" => false, "message" => "Item cannot be deleted");

        echo json_encode($response);
        exit;

    }

    public function Images($id){

        $data['pagetitle'] = "Add/Edit Sub Images";
        $data['id'] = $id;
        $data['item'] = FPModel::findOrfail($id);
        $data['images'] = FPModel::getFPImages($id);
        // $primary = FPModel::setPrimaryImage($id);
        // if (!$primary)
        //     print_r($primary);
        $data['formaction'] = "/admin/frontpage/add-image";
        $data['js'] = array("/js/frontpage.js");

        return view("admin.frontpage.subimages", $data);
    }

    public function addFPImage(Request $request){

        $id = $request->input("fpId");
        $fpimage = $request->file("fpimage");
        if (!$fpimage){
            return redirect('/admin/frontpage/images/add-edit/'.$id)
                    ->with('message', $this->displayError('You have not selected an image '));
        }

        try {
            $upload = new UploadLib;
            $upload->setFileTypes(array("jpg", "png", "jpeg", "gif"));
            $upload->setUploadPath("fpimages");
            $upload->setFileSizeLimit(self::FP_IMAGE_LIMIT);
            $result = $upload->AddImage($fpimage);
        } catch(\Exception $e) {
            return redirect('/admin/frontpage/images/add-edit/'.$id)
                    ->with('message', $this->displayError('Error: '.$e->getMessage()));
        }

        if ($result){
            $dbresult = FPModel::InsertFPImage($fpimage->getClientOriginalName(),
                                             $fpimage->getFileName().".".$fpimage->getClientOriginalExtension(),
                                             $id
                                             );
            if ($dbresult)
                return redirect("admin/frontpage/list")->with("message", $this->displaySuccess("Image added successfully"));
            else
                return redirect('/admin/frontpage/images/add-edit/'.$id)->with('message', "Something went wrong. Unable to add image");
        } else {
            return redirect('/admin/frontpage/images/add-edit/'.$id)->with('message', "Something went wrong. Unable to add image");
        }
    }

    public function SetPrimaryImg(Request $request){
        $image_id = $request->input("img_id");

        FPModel::setPrimaryImg($image_id);
        
        echo json_encode(array("message" => "This is primary image!"));
        exit;
    }

}
