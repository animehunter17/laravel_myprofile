<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use illuminate\Http\Request;
use Auth;
use App\Portfolio;
use App\Libraries\UploadLib;

class PortfolioController extends Controller {
    const PORT_IMG_LIMIT = 2000000;

    public function __construct(){
        $this->middleware("auth");

    }


    public function addnew(){

        $data['formaction'] = "/admin/portfolio/addnew";
        $data['pagetitle'] = "Add new portfolio";

        return view("admin.portfolio.form", $data);

    }

    public function processAddNew(Request $request){
        $check = $this->checkFormRequirements($request);
        if (is_array($check))
            return redirect('/admin/portfolio/addnew')->with("message", $check['message']);

        $title  = $request->input('title');
        $link   = $request->input('link');
        $pos    = $request->input('position');
        // $file   = $request->file('image');


        // $Path = $_SERVER['DOCUMENT_ROOT']."/media/";
        // if (!file_exists($Path))
        //     mkdir($Path, 0777, true);

        // $orig_filename = $file->getClientOriginalName();
        // $file->move($Path, $orig_filename."_".time());
        // print_r($file);
        Portfolio::add($title, $link, $pos);


        return redirect("/admin/portfolio/listing")->with( "message", "Portfolio Added!" );


    }


    public function listing()
    {
        $data['list'] = Portfolio::all();
        $data['pagetitle'] = "Portfolio listing";

        return view("admin.portfolio.list", $data);
    }


    private function allowedTypes($mimetype, $types=array())
    {
        if (empty($types)) {
            return;
        }

        foreach($types AS $type):
            if (strpos(strtolower($mimetype), $type) !== False) {
                return true;
            }

        endforeach;

        return false;
    }


    public function edit($id)
    {
        $data['item'] = Portfolio::getPortfolio($id);
        $data['formaction'] = "/admin/portfolio/process-update";
        $data['pagetitle'] = "Edit portfolio";
        return view("admin.portfolio.form", $data);

    }

    public function processUpdatePortfolio(Request $request)
    {
        $check = $this->checkFormRequirements($request);
        if (is_array($check))
            return redirect('/admin/portfolio/edit')->with("message", $check['message']);

        $update_data = ["port_title" => $request->input("title"), "port_link" => $request->input("link")];
        error_log("data: ".var_export($update_data, true));
        Portfolio::where("port_id", $request->input("id"))
                ->update($update_data);

        return redirect('/admin/portfolio/listing')->with("message", $this->displaySuccess("Successfully updated"));
        // Portfolio::update();

    }

    public function processDeletePortfolio(Request $request){



    }

    public function Images($id){

        $data['pagetitle'] = "Add/Edit Portfolio Images";
        $data['id'] = $id;
        // $data['item'] = FPModel::findOrfail($id);
        $data['images'] = Portfolio::getPortfolioImages($id);
        // $primary = FPModel::setPrimaryImage($id);
        // if (!$primary)
        //     print_r($primary);
        $data['formaction'] = "/admin/portfolio/add-image";
        $data['js'] = array("/js/portfolio.js");

        return view("admin.portfolio.images", $data);
    }

     public function addPortfolioImage(Request $request){

        $id = $request->input("portfolioId");
        $fpimage = $request->file("port_image");
 
        if (!$fpimage){
            return redirect('/admin/portfolio/images/add-edit/'.$id)
                    ->with('message', $this->displayError('You have not selected an image '));
        }

        try {
            $upload = new UploadLib;
            $upload->setFileTypes(array("jpg", "png", "jpeg", "gif"));
            $upload->setUploadPath("portfolio");
            $upload->setFileSizeLimit(self::PORT_IMG_LIMIT);
            $result = $upload->AddImage($fpimage);
        } catch(\Exception $e) {
            return redirect('/admin/portfolio/images/add-edit/'.$id)
                    ->with('message', $this->displayError('Error: '.$e->getMessage()));
        }

        if ($result){
            $dbresult = Portfolio::InsertImage($fpimage->getClientOriginalName(),
                                             $fpimage->getFileName().".".$fpimage->getClientOriginalExtension(),
                                             $id
                                             );
            if ($dbresult) {
                return redirect("admin/portfolio/listing")->with("message", $this->displaySuccess("Image added successfully"));
            }
        } 

        return redirect('/admin/portfolio/images/add-edit/'.$id)->with('message', "Something went wrong. Unable to add image");
    }

    public function deleteImage(Request $request)
    {
        try {
            Portfolio::DeleteImage($request->input("id"));
            $result['message'] = "Image Deleted";
            $result['success'] = true;
        } catch(Exception $e) {
            $result['message'] = $e->getMessage();
        }

        echo json_encode($result);
    }


    private function checkFormRequirements(Request $request)
    {
        if (!$request->input('title') && !$request->input('link')) {
            return array('message' => 'Please enter a title and link');
        }

        // if (!$request->hasFile("image"))
        //     return array('message' => 'Please upload an image');

        // $file = $request->file("image");
        // if ($file->getSize() > 800000)
        //     return array('message' => 'Filesize is too big');

        // if (!$this->allowedTypes($file->getMimeType(), array("gif", "jpg", "png", "jpeg")))
        //     return array('message' => 'Filetype NOT allowed!!');


    }

}
