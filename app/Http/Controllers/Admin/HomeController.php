<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use illuminate\Http\Request;
use Auth;
use App\Admin as AdminAction;
use App\Helpers\Ajax;
use DB;

class HomeController extends Controller {

    public function __construct(){
        $this->middleware("auth");
    }

    public function Index(Request $request){

        if (Auth::user()->isAdmin())
            echo "Im admin!";

        return view('admin.home');

    }

    public function MenuList(){

        // $data['menus'] = $this->getMenuList();
        $data['pagetitle'] = "Menu List";
        $data['js'] = [
                        "/js/menu.js", 
                        "/js/angular/controllers/menu.js", 
                        "/js/angular/services/menuService.js"];
        $data['ng_app'] = "menuApp";
        return view('admin.menulist', $data);

    }


    public function MenuSetup(){

        // echo "menu here";
        $data['menus'] = $this->getMenuList();
        $data['pagetitle'] = "Menu Setup";
        $data['js'] = array("/js/menu.js");
        return view('admin.menusetup', $data);

    }

    private function getMenuList(){

        $menu_data = AdminAction::all();
        return self::RebuildMenuList($menu_data->toArray());

    }
    

    public function AddMenu(Request $request){

        $menuName = $request->input("menu_name");
        $menuLink = $request->input("linkurl");
        $parent_id = $request->input("parent_id");

        if(!$menuName)
            return redirect('/admin/menu-setup')->with('error', 'Please make sure a menu name is entered');


        //Add Parent Menu
        $adminMenu = new AdminAction;
        if (!$menuLink && $menuName){
            
            $max_parent_id = DB::table("AdminMenu")->max("parent_id");
            $new_parent_id = $max_parent_id + 1;
            
            $adminMenu->parent_id = $new_parent_id;
            $adminMenu->link_name = $menuName;
            $adminMenu->save();

            return redirect('/admin/menu-setup')->with('error', 'Menu Added');

        }

        //Add Submenu
        if ($menuLink && $parent_id && $menuName){
            $max_submenu_id = DB::table("AdminMenu")->where("parent_id", $parent_id)->max("sub_id");
            $adminMenu->parent_id = $parent_id;
            $adminMenu->link_name = $menuName;
            $adminMenu->url = $menuLink;
            $adminMenu->sub_id = $max_submenu_id + 1;
            $adminMenu->save();
            
            return redirect('/admin/menu-setup')->with('error', 'Sub-Menu Added');

        }

    }

    public function GetAllMenus(){

        Ajax::is();
        $result['data'] = $this->getMenuList();

        Ajax::sendResponse($result);
    }


    public static function RebuildMenuList($menuData=array()){
        if(empty($menuData))
            return;

        $new_menu = array();
        foreach ($menuData AS $menu ):
            if (!$menu['sub_id']){
                $new_menu[$menu['parent_id']] = $menu;
            }
            
            if ( $menu['sub_id'] ){
                $new_menu[$menu['parent_id']]['submenus'][] = $menu;
            }

        endforeach;

        return $new_menu;
    }

    public function UpdateMenuItem(Request $request){
        
        Ajax::is();

        try {
            $menuId     = $request->input("menuid");
            $menuName   = $request->input("link_name");
            $url        = $request->input("suburl");

            AdminAction::UpdateMenuItem($menuId, $menuName, $url);
            Ajax::sendResponse();
        } catch (\illuminate\Database\QueryException $e) {
            $result['message'] = $e->getMessage();
            Ajax::sendResponse($result, false);
        }

        // return redirect('/admin/menu-list')->with('error', 'Menu Item updated');

    }

    public function DeleteMenu(Request $request){

        try {
            $menuId = $request->input("menuId");

            AdminAction::where("id", $menuId)->delete();
            Ajax::sendResponse();
        } catch (\illuminate\Database\QueryException $e) {
            $result['message'] = $e->getMessage();
            Ajax::sendResponse($result, false);
        }

    }



}

