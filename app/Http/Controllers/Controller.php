<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function displayError($msg){
        return "<div class='alert alert-danger'>".$msg."</div>";
    }

    public function displaySuccess($msg){
        return "<div class='alert alert-sucess'>".$msg."</div>";
    }


}
