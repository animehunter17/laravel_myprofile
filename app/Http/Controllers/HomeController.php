<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Frontpage AS FPModel;
use illuminate\Http\Request;
use App\Portfolio;
use App\Libraries\TwitterLib;
use Mail;
use Cache;


class HomeController extends Controller 
{
    public function Index()
    {
        if (!Cache::has("twitter_feeds")) {
            error_log("retreiving new tweets");
            $twitter = new TwitterLib;
            $twitter_data = $twitter->getTimeline();
            Cache::add("twitter_feeds", $twitter_data, 3);
        }

        $data['title'] = "sinluong.com";
        $data['portfolio'] = $this->sortPortfolio(Portfolio::getAll());
        $data['fp'] = FPModel::all();
        // print_R($data['fp']);
		$data['twitter'] = Cache::get("twitter_feeds");
        // error_log("twitter: ".var_export($data['twitter'], true));

        return View("frontpage", $data);
    }

    private function sortPortfolio($portfolio=array())
    {
        if (empty($portfolio)) {
            return $portfolio;
        }

        $data = array();
        $portId = 0;
        foreach($portfolio AS $item):
            //if portfolio Id is the same as previous Id AND is greater than 0 then add images into same array
            if ($item->port_id == $portId && $portId > 0){
                $data[$portId]['images'][] = $item->encrypt_name;
            //Otherwise create new array for new portfolio
            } else {
                $portId = $item->port_id;
                $data[$portId] = array(
                    "id" => $item->port_id,
                    "title" => $item->port_title,
                    "link" => $item->port_link,
                    "images" => array($item->encrypt_name)
                );
            }

        endforeach;

        return $data;

    }

    public function SendMsg(Request $request)
    {
        // $from = $request->input("email");
        // $to = "arlong2k8@gmail.com";
        // $headers = "From: ".$from." \r\n".
        //             "X-Mailer: PHP/".phpversion();
        // $subject = "New message from site";

        // mail($to, $subject, $request->input("message"), $headers); 
        Mail::send("emails.contact", ["msg" => $request->input("message"), "from" => $request->input("email") ], function($message) use($request){
            $message->from("noreply@sinluong.com");
            // $message->to("arlong2k8@gmail.com");
            // $message->bcc(["arlong2k8@gmail.com", "sluong@sinluong.com"]);
            $message->to(["arlong2k8@gmail.com", "sluong@sinluong.com"]);
            $message->subject("New message from site");

        });

        if ($request->input("copy")) {
            Mail::send("emails.contact", ["msg" => $request->input("message") ], function($message) use($request){
                $message->from("noreply@sinluong.com");
                // $message->to("arlong2k8@gmail.com");
                $message->to($request->input("email"));

                $message->subject("Copy Message: From sinluong.com");
            });
        }

        echo json_encode(array("success" => true));
        exit;
    }


}
