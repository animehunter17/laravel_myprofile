<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['middleware' => ['web']], function(){
    Route::get("/", "HomeController@Index");

    //contact
    Route::post("/contact/send-message", "HomeController@SendMsg");
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //admin functions
    Route::get("/admin/login", "Auth\AuthController@getLogin");
    Route::post("/admin/login", "Auth\AuthController@postLogin");
    Route::get("/admin/logout", "Auth\AuthController@getLogout");
    Route::get("/admin/home", array("as" => "home", "uses" => "Admin\HomeController@Index"));
    Route::get("/admin/menu-setup", array("as" => "menu", "uses" => "Admin\HomeController@MenuSetup"));
    Route::get("/admin/menu-list", "Admin\HomeController@MenuList");
    Route::post("/admin/addMenu", "Admin\HomeController@AddMenu");
    Route::post("/admin/update-menu", "Admin\HomeController@UpdateMenuItem");
    Route::post("/admin/delete-menu", "Admin\HomeController@DeleteMenu");
    Route::get("/admin/menu/get-list", "Admin\HomeController@GetAllMenus");

    //frontpages
    Route::get("/admin/frontpage/addnew", "Admin\FrontpageController@addnew");
    Route::get("/admin/frontpage/edit/{id}", "Admin\FrontpageController@edit");
    Route::post("/admin/frontpage/addnew", "Admin\FrontpageController@processAddNew");
    Route::post("/admin/frontpage/update/{id}", "Admin\FrontpageController@processUpdate");
    Route::post("/admin/frontpage/delete-window", "Admin\FrontpageController@processDelete");

    Route::get("/admin/frontpage/list", "Admin\FrontpageController@listing");
    Route::get("/admin/frontpage/images/add-edit/{id}", "Admin\FrontpageController@Images");
    Route::post("/admin/frontpage/add-image", "Admin\FrontpageController@addFPImage");
    Route::post("/admin/frontpage/image/make-primary", "Admin\FrontpageController@SetPrimaryImg");

    //portfolio
    Route::get("/admin/portfolio/addnew", "Admin\PortfolioController@addnew");
    Route::post("/admin/portfolio/addnew", "Admin\PortfolioController@processAddNew");
    Route::get("/admin/portfolio/listing", "Admin\PortfolioController@listing");
    Route::get("/admin/portfolio/edit/{id}", "Admin\PortfolioController@edit");
    Route::get("/admin/portfolio/images/add-edit/{id}", "Admin\PortfolioController@Images");
    Route::post("/admin/portfolio/add-image", "Admin\PortfolioController@addPortfolioImage");
    Route::post("/admin/portfolio/process-update", "Admin\PortfolioController@processUpdatePortfolio");
    Route::post("/admin/portfolio/delete-image", "Admin\PortfolioController@deleteImage");


});


