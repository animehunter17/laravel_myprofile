<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Admin extends Model
{
    //
    protected $table = "AdminMenu";


    public static function updateMenuItem($menuId, $menuName, $url){
        
        DB::table("AdminMenu")
                ->where("id", $menuId)
                ->update(["link_name" => $menuName, "url" => $url]);

        return true;

    }

}
