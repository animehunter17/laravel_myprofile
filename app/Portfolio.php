<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Portfolio extends Model
{
    //
    protected $table = "portfolio";
    protected static $p_img_tbl = "portfolio_images";

    public static function add($title, $link, $pos)
    {
        $insertdata = ["port_title" => $title, "port_link" => $link, "position" => $pos];
        DB::table("portfolio")->insert($insertdata);
    }

    public static function getPortfolio($id)
    {
        return DB::table("portfolio")->where("port_id", $id)->first();
    }

    public static function updatePortfolio(){


    }

    public static function InsertImage($original_name, $encrypt_name, $pid)
    {
        $insertdata = ["original_name" => $original_name, "encrypt_name" => $encrypt_name, "portfolio_id" => $pid];

        return DB::table(self::$p_img_tbl)->insert($insertdata);
    }

    public static function getPortfolioImages($id)
    {
        return DB::table(self::$p_img_tbl)->where("portfolio_id", $id)->get();
    }

    public static function DeleteImage($id)
    {
        return DB::table(self::$p_img_tbl)->where("id", $id)->delete();
    }

    public static function getAll()
    {
        return DB::table("portfolio")
                    ->leftJoin(self::$p_img_tbl, "port_id", "=", "portfolio_id")
                    ->select("port_id", "port_title", "port_link", "encrypt_name")
                    ->get();

    }

}
