<?php

namespace app\Libraries;
// use App\Exceptions\Handler;
use \Exception;

class UploadLib {

    public $filetypes;
    public $filesizeLimit;
    public $uploadPath;

    private $mediapath;

    public function __construct(){
        // $this->mediapath = $_SERVER['DOCUMENT_ROOT']."/media/";
        $this->mediapath = "./media/";
    } 

    public function AddImage($image){

        if (!in_array($image->getClientOriginalExtension(), $this->filetypes))
            throw new Exception("File type not allowed: ".$image->getClientOriginalExtension());

        if ($image->getSize() > $this->filesizeLimit)
            throw new Exception("File size limit exceeded");

        $encrypt_name = $image->getFileName().".".$image->getClientOriginalExtension();
        return $image->move($this->uploadPath, $encrypt_name);
    }

    public function setUploadPath($path){
        $new_path = $this->mediapath.$path;
        if (!$path || !is_string($path) || !file_exists($new_path))
            throw new Exception("Invalid path: ".$new_path);

        $this->uploadPath = $new_path;

    }

    public function setFileTypes($filetypes=array()){
        
        if (!empty($filetypes))
            $this->filetypes = $filetypes;

    }

    public function setFileSizeLimit($limit){
        if ($limit && is_numeric($limit))
            $this->filesizeLimit = $limit;

    }
}
