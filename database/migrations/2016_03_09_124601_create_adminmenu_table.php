<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminmenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AdminMenu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->integer('sub_id')->nullable();
            $table->string('link_name', 30);
            $table->string('url')->nullable();
            $table->integer('order_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('AdminMenu');
    }
}
