<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fpphotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FPphotos', function (Blueprint $table) {
            $table->increments('photo_id');
            $table->string("original_name", 255);
            $table->string("encrypt_name", 255);
            $table->integer("window_id");
            $table->boolean("primary")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('FPphotos');
    }
}
