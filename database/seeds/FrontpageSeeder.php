<?php

use Illuminate\Database\Seeder;

class FrontpageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("frontpage")->insert([[
                "title" => "About Me",
                "sub_title" => "Full LAMP Stack Developer",
                "desc1" => "A PHP developer who's keen and passionate and always looking forward to learning new technologies.",
                "desc2" => "I.T Background: 
                I've previously come from an IT Networking/Support environment and I've developed a great amount of skills and knowledge from the previous jobs I have worked for and courses that I took. Having worked in this field for 4 years, this ended for me in Dec 2010. Since then I went on to pursue a Web Design & Development career.

                I specialise in these technologies, PHP w/ MySQL, MVC Framework Codeigniter, AJAX, Javascript, Jquery, HTML/XHTML, CSS and I've been involved with a project that uses WordPress.",
            ], [
                "title" => "Photos",
                "sub_title" => "Past & Present",
                "desc1" => "Gallery of my favourite photos.",
                "desc2" => "I.T Background:Gallery of my favourite <a href='photos'>Photos</a>.
                Enjoy! =)"
            ], [
                "title" => "Photos",
                "sub_title" => "Past & Present",
                "desc1" => "Gallery of my favourite photos.",
                "desc2" => "I.T Background:Gallery of my favourite <a href='photos'>Photos</a>."
            ], [
                "title" => "Contact",
                "sub_title" => "Send Me a MSG",
                "desc1" => "If you have any questions, send me a message and I'll get back to you as soon as I can. =)",
                "desc2" => "If you have any questions, send me a message and I'll get back to you as soon as I can. =)"

            ]]);
    }
}
