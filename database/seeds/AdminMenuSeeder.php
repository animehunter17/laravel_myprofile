<?php

use Illuminate\Database\Seeder;

class AdminMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("AdminMenu")->insert([[
            "parent_id" => 1,
            "sub_id"    => NULL,
            "link_name" => "Admin Menu",
            "url"       => NULL,
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        ], [
            "parent_id" => 2,
            "sub_id"    => NULL,
            "link_name" => "Front page",
            "url"       => NULL,
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ], [
        
            "parent_id" => 1,
            "sub_id"    => 1,
            "link_name" => "Home",
            "url"       => "/admin/home",
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ], [
            "parent_id" => 2,
            "sub_id"    => 1,
            "link_name" => "Add New",
            "url"       => "/admin/frontpage/addnew",
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ], [
            "parent_id" => 2,
            "sub_id"    => 2,
            "link_name" => "Window List",
            "url"       => "/admin/frontpage/list",
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ], [
            "parent_id" => 3,
            "sub_id"    => NULL,
            "link_name" => "Portfolio",
            "url"       => NULL,
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ], [
            "parent_id" => 3,
            "sub_id"    => 1,
            "link_name" => "new portfolio",
            "url"       => "/admin/portfolio/addnew",
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ], [
            "parent_id" => 3,
            "sub_id"    => 2,
            "link_name" => "Portfolio list",
            "url"       => "/admin/portfolio/listing",
            "order_no"  => NULL,
            "created_at" => date("Y-m-d H:i:s")
        
        ]]);
    }
}
