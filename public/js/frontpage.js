var Frontpage = (function(){
    "use strict"

    var MakeImgPrimary = function(){
        $(document).on('click', 'button.make-primary', function(){
            var $button = $(this);
            var photo_id = $button.data("photo-id");
            var data = { img_id: photo_id, _token: $('input[name=_token]').val() };
            var request = ajaxReq("/admin/frontpage/image/make-primary", data);
            request.done(function(data){
                alert(data.message);
                $('.fp-image-container').each(function(){
                    // console.log($(this));
                    // if element does not have button then add it.
                    if (!$(this).has("button").length){
                        $(this).append("<button class='btn btn-primary make-primary'>Make primary</button>");
                    } else {
                        $(this).find("button").show();
                    }
                });
                $button.hide();
            });
        });
    }

    var deleteWindow = function(){
        $(".fp-list a.delete").on("click", function(){
            var response = confirm("Delete this window?");
            if (response){
            //     alert("deleted");
                var $div = $(this).parent("div");
                var id = $div.data("id");
                var data = { "id": id, _token: $('meta[name=csrf_token]').attr("content") };
                var request = ajaxReq("/admin/frontpage/delete-window", data);
                request.done(function(data){
                    alert(data.message);
                    $div.slideUp();
                
                });
            }


        });
   }

   function ajaxReq(url, data){

        return $.ajax({
                    method: "POST",
                    data: data,
                    dataType: "json",
                    url: url
                });
    }


    var init = function(){
        MakeImgPrimary();
        deleteWindow();
    }

    return {
        init: init

    }


})();
Frontpage.init();
