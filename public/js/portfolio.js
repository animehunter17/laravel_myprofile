var Portfolio = (function(){
    "use strict"

    var MakePrimary = function(){
        $('button.make-primary').on('click', function(){
            alert("data");
        });
    }
    
    var deleteImage = function(){
        $('button.delete-img').on('click', function(){
            var confirmDel = confirm("Are you sure you want to delete this image?");
            if (confirmDel){
                var $imgDiv = $(this).closest("div");
                var id = $imgDiv.data("photo-id");
                var data = { "id": id, "_token": $('input[name=_token]').val() };
                var response = ajaxReq("/admin/portfolio/delete-image", data);
                response.success(function(data){
                    alert(data.message);
                    // console.log(id);
                    if (data.success)
                        $imgDiv.fadeOut(500);
                });
            }

        });

    }

    function ajaxReq(url, data){
        return $.ajax({
                url: url,
                data: data,
                type: "POST",
                dataType: "json"
            });
    }

    var init = function(){
        MakePrimary();
        deleteImage();
    }

    return {
        init: init

    }

})();
Portfolio.init();
