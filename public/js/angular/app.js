var fpApp = angular.module("fpApp", ["fpCtrl"], function($interpolateProvider){
    $interpolateProvider.startSymbol("<%");
    $interpolateProvider.endSymbol("%>");
});
