angular.module("menuService", [])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }])
    .factory("Menu", function($http){

        return {
            getMenuList: function(){
                return $http.get("/admin/menu/get-list").then(function(data){
                    return data.data;  
                });
            }
        }

    });
