angular.module("menuApp", ["menuService"], function($interpolateProvider){
        $interpolateProvider.startSymbol("<%");
        $interpolateProvider.endSymbol("%>");
    })
    .controller("menuController", function($scope, Menu){

        $scope.menulist = {};

        Menu.getMenuList().then(function(data){
            var newList = [];
            angular.forEach(data.data, function(thisMenu){
                thisMenu.show = false; 
                newList.push(thisMenu);
            });
            console.log(newList);
            $scope.menulist = newList;
        });

        $scope.deleteSubMenu = function(menuId){
            console.log("deleting");
            console.log(menuId);

        }

        $scope.updateMenu = function(){
            var postdata = {
                    "id": $scope.menuId,
                    "link_name": $scope.linkName,
                    "link_url" : $scope.link_url
            }
            console.log(postdata);
        }

        $scope.showHideMenu = function(menu){
            // console.log(menu);
            if (menu.show)
                menu.show = false;
            else
                menu.show = true;
        }

        $scope.showMenuDialog = function(menu){
            $scope.linkName = menu['link_name'];
            $('#parent_dialog').dialog("open");
        }

        $scope.showSubDialog = function(submenu){
            // console.log(submenu);
            $scope.linkName = submenu['link_name'];
            $scope.sub_url = submenu['url'];
            $('#sub_dialog').dialog("open");
        }

        $(document).ready(function(){
            $( "#sub_dialog, #parent_dialog" ).dialog({
                autoOpen: false,
                width: 450,
                show: { effect: "blind", duration: 1000 },
                hide: { effect: "explode", duration: 1000 }

            });
        });

    });
