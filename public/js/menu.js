var Menu = (function(){
    "use strict"

    var showHideMenu = function(){
        $('a.show_parent').on("click", function(){
            var sub_window = $(this).parents('.title_parent').next(".sub-menu");

            if (sub_window.is(":visible"))
                sub_window.slideUp();
            else
                sub_window.slideDown();

        });
    }


    var MenuDialog = function(){
        $(document).ready(function(){
            $( "#sub_dialog, #parent_dialog" ).dialog({
                autoOpen: false,
                width: 450,
                show: { effect: "blind", duration: 1000 },
                hide: { effect: "explode", duration: 1000 }

            });
        });

    }


    //Edit Parent Menu
    var editParentMenu = function (){
        $('a.edit_parent').click(function(){
            var parentid = $(this).parents('.title_parent').data("parent-id");
            var parentName = $(this).parents('.title_parent').find(".menu-title").text();

            $('#parent_dialog input[name=menuid]').val(parentid);
            $('#parentname').val(parentName);
 
            $('#parent_dialog').dialog('open');
        });
    }

    var SubMenuDialog = function(){
        //edit the sub menu
        $('a.edit-submenu').click(function(){
            var subMenuId = $(this).parents('div.list').data("submenu-id");
            var subMenuName = $(this).parents('div.list').find(".sub-menu-title").text();
            $('#menuid').val(subMenuId);
            $('#subname').val(subMenuName);
            $('#suburl').val($(this).data('submenu-url'));
 
            $('#sub_dialog').dialog('open');
        });
    }

    var deleteFP = function(){
        $('a.delete').on("click", function(){
            var response = confirm("Delete this item?");
            if (response) {
                var $div = $(this).parent("div.list_div");
                var _token = $('meta[name=csrf_token]').attr('content');
                var data = { "id": $div.data('id'), "_token": _token };
                var request = ajaxReq("/admin/frontpage/delete", data);
                // alert(request);
                request.done(function(data){
                    if (data.success){
                        fadeOutMsg(data.message);
                        $div.slideUp();

                    }
                });
            }
        });

    }


    var init = function(){
        showHideMenu();
        MenuDialog();
        editParentMenu();
        SubMenuDialog();
        deleteFP();

    }

    function ajaxReq(url, data){

        return $.ajax({
                    method: "POST",
                    data: data,
                    dataType: "json",
                    url: url
                });
    }

    function fadeOutMsg(msg){
        $('.page_title').after('<div class="alert alert-success">' + msg + '</div>');
        $('div.alert').fadeOut(3000);
    }


    return {
        init: init

    }


})();
Menu.init();
