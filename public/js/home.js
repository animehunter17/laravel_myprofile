var Home = (function(){
    var win_width;
    var win_count =0;
    var nav_width = $('ul.nav-pills').width();
    var current_win_id = 1;
    var prev_win_id = 1;
    var section_widths;
    var $sections = $('section');

    var onLoad = function(){
        $(document).on("ready", function(){
            win_width = $(window).width();
            section_widths = DefaultSectionWidths(win_width);
            var i =1;
            $sections.each(function(){
                // $(this).css("width", win_width + nav_width);
                $(this).css("width", win_width );
                $(this).css("margin-left", section_widths[i]);
                win_count ++;
                i++;
            });

            $('html').css("width", win_width * win_count);
        });
    }

    var resize = function(){
        $(window).resize(function(){
            win_width = $(window).width();
            $sections.css("width", win_width );
            $('html').css("width", win_width * win_count);

            recalculateMargins();
            // console.log(section_widths);
        });
    }

    var LoadPage = function(){
        var timer = 0;
        var delay = 400;
        var prevent = false;
        var $links = $('.nav-pills li a');

        //one click
        $links.on('click', function(){
            $links.removeClass("active");
            $(this).addClass("active");
            current_win_id = $(this).data("id");
            timer = setTimeout(function(){
                if(!prevent){
                    //if current win is same or greater then moving forward
                    // console.log("current: " + current_win_id);
                    // console.log("previous: " + prev_win_id);
                    if (current_win_id >= prev_win_id) {
                        var slideDiff = (current_win_id - prev_win_id) + 1;
                        var forward = true;
                    } else {
                        var slideDiff = (Math.abs(current_win_id - prev_win_id) + 1);
                        var forward = false;
                    }
                    prev_win_id = current_win_id;
                    
                    //get size
                    var widthDiff = section_widths[slideDiff]; 
                    // console.log("diff: " + widthDiff);
                    // console.log("forward: " + forward);

                    calculateSectionWidths(widthDiff, forward);
                }
                prevent = false;
            }, delay);
        //double click
        }).on("dblclick", function(){
            clearTimeout(timer);
            prevent = true;
            // return false;  
            $(this).click();
        });
    }


    var sendMsg = function(){
        $("button.sendmsg").on("click", function(e){
             var isvalidate = $("#myform").valid();
             if (isvalidate){
                e.preventDefault();
                // $('section.Contact > form').validate();
                var $email = $('input[name=email]');
                var $message = $('textarea[name=message]');
                var copy_checkbox = $('input[name=copy]');
                var data = { 
                            "_token": $('meta[name=csrf_token]').attr("content"),
                            "email": $email.val(), 
                            "message": $message.val(),
                            "copy": (copy_checkbox.is(":checked")) ? true : false
                            };
                console.log(data);
                var request = ajaxReq("/contact/send-message", data);
                request.done(function(data){
                    alert("Message sent!");
                    $email.val('');
                    $message.val('');
                    
                });
             }
        });
    }

    var calculateSectionWidths = function(widthDiff, forward){
        // var new_widths = new Array();
        var i = 1;
        $sections.each(function(){
            var old_margin = $(this).css("margin-left").replace("px", "");

            // divWidth = i * (parseInt(old_margin) + parseInt(widthDiff));
            if (forward){
                var new_margin = old_margin - widthDiff;
            } else {
                var new_margin = parseInt(old_margin) + parseInt(widthDiff);
            }

            section_widths[i] = (i - 1) * win_width;
            $(this).animate({'marginLeft': new_margin + 'px'}, 500);
            i++;
        });
        // console.log(section_widths);
        // console.log(section_widths);
    }
    
    function recalculateMargins(){
        var thisMargin;
        var divDiff;
        var i =1;
        var $activelink = $('.nav-pills li a.active').data("id");
        $sections.each(function(){
            divDiff = Math.abs(i - $activelink);
            // console.log(divDiff);
            thisMargin = 0;
            if (i < $activelink) {
                thisMargin = 0 - (win_width * divDiff);
            } 
            if (i > $activelink) {
                thisMargin = 0 + (win_width * divDiff);
            }
            $(this).css("margin-left", thisMargin);
            // section_widths[i] = thisMargin;
            section_widths[i] = (i - 1) * win_width;
            i++;
        });
        // console.log(section_widths);
    }

    function DefaultSectionWidths(win_width){
        var i = 1;
        var sections = new Array();
        $('.nav-pills li a').each(function(){
            sections[i] = (parseInt($(this).data("id")) * win_width - win_width); 
            i++;
        });
        // console.log(sections);
        return sections;
    }

    function ajaxReq(url, data){

        return $.ajax({
                url: url,
                type: "json",
                method: "POST",
                data: data

        });
    }


    var init = function(){
        onLoad();
        LoadPage();
        resize();
        sendMsg();
    }


    return {
        init: init
    }

})();

Home.init();
